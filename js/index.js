$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval:3000
    })
})

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
})

$(function(){
    $('#alergenos').on('show.bs.modal', function(e){
      console.log('Inicio del modal');

      $('#botonAlergenos').removeClass('boton-alergenos');
      $('#botonAlergenos').addClass('btn-sencondary');
      $('#botonAlergenos').prop('disabled', true);

    })
    $('#alergenos').on('shown.bs.modal', function(e){
      console.log('El modal se mostró');
    })
    $('#alergenos').on('hide.bs.modal', function(e){
      console.log('Minimizamos el modal');
  
      $('#botonAlergenos').removeClass('btn-secondary');
      $('#botonAlergenos').addClass('boton-alergenos');
      $('#botonAlergenos').prop('disabled', false);

    })
    $('#alergenos').on('hidden.bs.modal', function(e){
      console.log('El modal desapareció');
    })
  });